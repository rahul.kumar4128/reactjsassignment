import React, { Component } from 'react';
import axios from 'axios'
// import axios from 'axios'
import '../App.css'
// import SignUp from './signup';
// import { Link, Route, BrowserRouter as Router, Switch } from 'react-router-dom';
class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            password: ''
        }
    }
    changeEvent(event) {
        // console.log(event.target.name);
        // console.log(event.target.value);
        const name = event.target.name;
        const value = event.target.value;
        this.setState({ [name]: value });
        // console.log(this.state);


    };
    submitEvent(event) {
        event.preventDefault();
        console.log(this.state);

        axios.get('http://localhost:8080/user/login',this.state)
            .then((response) => {
                console.log(response)
            })
            .catch(function (error) {
                console.log(error)
            })
    }
    render() {
        return (<div className="login">
            <form onSubmit={this.submitEvent.bind(this)} className="form">
                <h1>Login</h1>
                <hr />
                <label htmlFor="name">User Name:-</label><br />
                <input type="text" name="name" id="name" placeholder="enter your Name" onChange={this.changeEvent.bind(this)} required /><br />
                <label htmlFor="password">Password:-</label><br />
                <input type="password" name="password" placeholder="enter your password" id="password" onChange={this.changeEvent.bind(this)} required /><br />
                <input type="submit" value="Login" id="loginBtn" />
                <hr />
            </form>
            {/* <Router>
                <Link to='/signup'><span>click here for signup</span></Link>
                <Switch>
                    <Route component={SignUp} path='/signup' exact strict />
                </Switch> */}
            {/* </Router> */}
        </div>);
    }
}

export default Login;