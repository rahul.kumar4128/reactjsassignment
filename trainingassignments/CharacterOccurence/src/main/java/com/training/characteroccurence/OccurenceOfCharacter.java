/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.training.characteroccurence;

import java.util.*;

/**
 *
 * @author aakriti
 */
public class OccurenceOfCharacter {

    public static Map occurence = new HashMap();//This map stores character and their ocuurences in a sentence.

    public static Map occurenceOfCharcters(char[] arr) {//This function takes a char array as input and stores character and occurence of each character in map
        int size = arr.length;
        arr = UpperToLowerCase(arr, size);//uppercase characters are converted to lowercase characters

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == ' ') {
                for (int k = i; k < size - 1; k++) {
                    arr[k] = arr[k + 1];
                }
                i--;
                size--;
            }
        }//Space is removed from sentence

        for (int startIndex = 0; startIndex < size; startIndex++) {//counting the number of coocurence in new array
            int count = 1;

            for (int j = startIndex + 1; j < size; j++) {

                if (arr[startIndex] == arr[j]) {
                    count++;
                    for (int m = j; m < size - 1; m++) {
                        arr[m] = arr[m + 1];
                    }
                    j--;
                    size--;
                }
            }
            occurence.put(arr[startIndex], count);

        }
        return occurence;
    }

    public static char[] UpperToLowerCase(char[] arr, int size) {//This function accepts array and converts all UppercaseToLowercase
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] >= 65 && arr[i] <= 90) {
                arr[i] += 32;
            }
        }
        return arr;
    }
}
