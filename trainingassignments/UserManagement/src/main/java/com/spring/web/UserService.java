package com.spring.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserService {
	
	@Autowired
	private UserRepository userRepository;
	
	public User save(User user) {
		return userRepository.save(user);
	}
	
	public List<User> findAll(){
		return userRepository.findAll();
	}
	
	public User findByNameAndPassword(String name,String password) {
		return userRepository.findByNameAndPassword(name,password);
	}
	
	public Boolean delete(Long id) {
		userRepository.deleteById(id);
		return true;
	}
	
	
	

}
