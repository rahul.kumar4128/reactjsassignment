/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.training.transaction;

import java.util.*;

/**
 *
 * @author aakriti
 */
public class Order {

    private String orderId;
    private Date orderDate;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        Cart cart = new Cart();
        if (cart.getNumberOfProducts() >= 3) {
            this.orderId = orderId;
        }
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

}
