/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genericstack.stack;

/**
 *
 * @author aakriti
 */
public class Book {

    String bookName;
    String authorName;
    double price;

    Book(String name, String authorName, double price) {
        this.bookName = name;
        this.authorName = authorName;
        this.price = price;
    }

    @Override
    public String toString() {
        return "Book Name : " + bookName + "\n" + "Author Name : " + authorName + "\n" + "Price : " + price + "\n";
    }

}
